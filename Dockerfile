FROM --platform=linux/amd64 debian:12 as build
ADD https://download.freebsd.org/releases/amd64/amd64/13.1-RELEASE/base.txz /
WORKDIR /src
RUN apt-get update -qyy && apt-get install xz-utils -qyy && tar xvf /base.txz -C /src

FROM --platform=freebsd/amd64 scratch

COPY --from=build /src /
